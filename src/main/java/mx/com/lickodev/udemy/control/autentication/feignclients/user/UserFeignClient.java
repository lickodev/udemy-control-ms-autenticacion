package mx.com.lickodev.udemy.control.autentication.feignclients.user;

import mx.com.lickodev.udemy.control.autentication.interceptors.AuthForwardInterceptor;
import mx.com.lickodev.udemy.control.commons.entity.users.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static mx.com.lickodev.udemy.control.commons.constants.Paths.*;

@FeignClient(name = "udemy-control-ms-users", configuration = AuthForwardInterceptor.class)
public interface UserFeignClient {

    @GetMapping(value = USERS_BASE_PATH + "/" + SEARCH_PATH + "/" + USERS_FIND_BY_USER_NAME_PATH)
    User findByUsername(@RequestParam String userName);

}
